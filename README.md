### SWP SS20: Supervised and unsupervised machine learning in the science of behaviour
# Detection of Pollen Foragers – Group 2: Tim Feige, Mara Kortenkamp

Based on: https://docs.google.com/document/d/1k1hzXBiak9c3yoDUznVZRlRxYWgI2q5y9RgWvofalLc/edit#heading=h.7exhv13gzmb0


## 1. Data
* We did the data labeling together with another group and used supervisely to label the pollen baskets in the videos.
* Every second frame of the videos was uploaded to supervisely and each pollen basket was marked with a single point. 
* https://app.supervise.ly/projects/76211/datasets

## 2. Environment
* We decided to write our code in a jupyter notebook: bee-proyject.ipynb. We will use python and especially the pytorch library.  
(We actively decided not to use google colab, since we haven't had a good experiences using it for larger projects.)

## 3. Components
This project is based on three components. The processing of labeled data, the implementation of a neural network and the evaluation of the networks performance.

### Network
The network is a fully convolutional neural network (FCNN) based on torch. The network has 7 layers with mutliple channels. You can find the implementation here: [pollen_network.ipynb](https://gitlab.com/TimKFeige/pollen-detection/-/blob/master/pollen_network.ipynb).  
The networks structure:
* 1st layer: 1 input channel, 30 output channel, kernel-size of 2 and stride of 2
* 2nd layer: 30 input channel, 30 output channel, kernel-size of 5 and stride of 2
* 3rd layer: 30 input channel, 60 output channel, kernel-size of 3 and stride of 1
* 4th layer: 60 input channel, 60 output channel, kernel-size of 3 and stride of 2
* 5th layer: 60 input channel, 120 output channel, kernel-size of 3 and stride of 1
* 6th layer: 120 input channel, 120 output channel, kernel-size of 3 and stride of 1
* 7th layer: 120 input channel, 1 output channel, kernel-size of 1 and stride of 1

We initialise the convolutional neural network, load the data with 4 dimensions and train the network. For its own validation it will compute its mean validation loss, the F1-Score and the accuracy. The number and sizes of layers are based on trial and error, as well as the choice of the optimizer and criterion. The mean square error appears to be the most promising criterion as well as the SGD the most promising optimizer.


### Data Processing
You can find the implementation here: [data_processing.ipynb](https://gitlab.com/TimKFeige/pollen-detection/-/blob/master/data_processing.ipynb).  
It is responsible for extracting images of single objects from given data. To process our data we implemented an general purpose extractor for supervise.ly data. This extractor distinguishes between several label types, such as point, polylines, polygons, cuboids, etc. It creates an individual directory to store the data, including an annotation file in COCO format, a labelstrength pickle file, directories for each class and if required a directory for a counterclass.
We crop images around the centre of given objects and apply data-augmentation to increase the datasample size. Such as rotation(in this case we rotate in steps of 60 degree to retain the honeycomb pattern in the background), flipping, blur and noise effects. By shifting the centre of the cropped images randomly and using 2D normal distribution accordingly, we increased the variety of our samples.


### Evaluation
You can find the implementation of the evaluation here: [evaluation.ipynb](https://gitlab.com/TimKFeige/pollen-detection/-/blob/master/Evaluation.ipynb).  
This will load the trained network and evaluate the performance, create heatmaps and plot those for an easier visualisation. As a result we have none max suppression heatmaps of each image, coordinations of existing and predicted objects. We plot the most extreme classifications such as true positive, false positive and false negative predictions. Due to technical limitations the number of epochs is set to 10, whereas we expect a much higher accuracy with more epochs.

